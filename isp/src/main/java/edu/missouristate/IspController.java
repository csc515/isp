package edu.missouristate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IspController {

	@GetMapping(value="/")
	public String getIndex() {
		return "isp";
	}
	
	@GetMapping(value="/privacy")
	public String getPrivacy() {
		return "privacy";
	}
	
}
