<!-- 
	anchor tags, resources/css, resources/js, resources/img, resources/fonts
	YOU NEED TO ADD THE CONTEXT PATH
 -->
<!DOCTYPE html>
<html lang="en">
<%@ include file="/WEB-INF/views/inc/head.jsp"%>
<body>
	<%@ include file="/WEB-INF/views/inc/header.jsp"%>
    <div class="container">
        <main role="main" class="pb-3">
            <div class="text-center">
                <h1 class="display-4">Welcome</h1>
                <p>Learn about <a href="https://docs.microsoft.com/aspnet/core">building Web apps with ASP.NET Core</a>.</p>
            </div>
        </main>
    </div>
	<%@ include file="/WEB-INF/views/inc/footer.jsp"%>
    <script src="<%=request.getContextPath() %>/resources/lib/jquery/dist/jquery.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/js/site.js?v=4q1jwFhaPaZgr8WAUSrux6hAuh0XDg9kPS3xIVq36I0"></script>
</body>
</html>
